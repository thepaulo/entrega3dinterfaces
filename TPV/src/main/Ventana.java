package main;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Ventana extends JPanel implements ActionListener{
	
	JButton a0Btn = new JButton("0");
	JButton a1Btn = new JButton("1");
	JButton a2Btn = new JButton("2");
	JButton a3Btn = new JButton("3");
	JButton a4Btn = new JButton("4");
	JButton a5Btn = new JButton("5");
	JButton a6Btn = new JButton("6");
	JButton a7Btn = new JButton("7");
	JButton a8Btn = new JButton("8");
	JButton a9Btn = new JButton("9");
	
	JLabel txt1 = new JLabel("Productos Totales:");
	JTextArea infoArea = new JTextArea();
	JTextArea iTxt = new JTextArea();
	
	JButton cocacolaBtn = new JButton(new ImageIcon("cocacola.jpg"));
	JButton fantaBtn = new JButton(new ImageIcon("fanta.jpg"));
	JButton bocadilloBtn = new JButton(new ImageIcon("bocadillo.jpg"));
	JButton aguaBtn = new JButton(new ImageIcon("agua.jpg"));
	JButton patatasBtn = new JButton(new ImageIcon("patatas.jpg"));
	JButton croquetasBtn = new JButton(new ImageIcon("croquetas.jpg"));
	JButton cafeBtn = new JButton(new ImageIcon("cafe.jpg"));
	JButton pizzaBtn = new JButton(new ImageIcon("pizza.jpg"));
	JButton heladoBtn = new JButton(new ImageIcon("helado.jpg"));
	
	JButton nuevoBtn = new JButton("Nuevo");
	JButton salirBtn = new JButton("Salir");
	
	
	 
     
	public Ventana(){
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 12, 0, 21, 0};
		gridBagLayout.rowHeights = new int[]{15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		
		GridBagConstraints gbc_txt1 = new GridBagConstraints();
		gbc_txt1.fill = GridBagConstraints.BOTH;
		gbc_txt1.insets = new Insets(0, 0, 5, 5);
		gbc_txt1.gridx = 1;
		gbc_txt1.gridy = 0;
		add(txt1, gbc_txt1);
		
		
		GridBagConstraints gbc_infoArea = new GridBagConstraints();
		gbc_infoArea.gridheight = 4;
		gbc_infoArea.gridwidth = 3;
		gbc_infoArea.insets = new Insets(0, 0, 5, 5);
		gbc_infoArea.fill = GridBagConstraints.BOTH;
		gbc_infoArea.gridx = 1;
		gbc_infoArea.gridy = 1;
		add(infoArea, gbc_infoArea);
		
		
		GridBagConstraints gbc_iTxt = new GridBagConstraints();
		gbc_iTxt.gridheight = 4;
		gbc_iTxt.gridwidth = 3;
		gbc_iTxt.insets = new Insets(0, 0, 5, 5);
		gbc_iTxt.fill = GridBagConstraints.BOTH;
		gbc_iTxt.gridx = 4;
		gbc_iTxt.gridy = 1;
		add(iTxt, gbc_iTxt);
		
		
		GridBagConstraints gbc_cocacolaBtn = new GridBagConstraints();
		gbc_cocacolaBtn.gridheight = 2;
		gbc_cocacolaBtn.fill = GridBagConstraints.BOTH;
		gbc_cocacolaBtn.insets = new Insets(0, 0, 5, 5);
		gbc_cocacolaBtn.gridx = 1;
		gbc_cocacolaBtn.gridy = 5;
		add(cocacolaBtn, gbc_cocacolaBtn);
		
		
		GridBagConstraints gbc_fantaBtn = new GridBagConstraints();
		gbc_fantaBtn.gridheight = 2;
		gbc_fantaBtn.fill = GridBagConstraints.BOTH;
		gbc_fantaBtn.insets = new Insets(0, 0, 5, 5);
		gbc_fantaBtn.gridx = 2;
		gbc_fantaBtn.gridy = 5;
		add(fantaBtn, gbc_fantaBtn);
		
		
		GridBagConstraints gbc_bocadilloBtn = new GridBagConstraints();
		gbc_bocadilloBtn.gridheight = 2;
		gbc_bocadilloBtn.fill = GridBagConstraints.BOTH;
		gbc_bocadilloBtn.insets = new Insets(0, 0, 5, 5);
		gbc_bocadilloBtn.gridx = 3;
		gbc_bocadilloBtn.gridy = 5;
		add(bocadilloBtn, gbc_bocadilloBtn);
		
		
		GridBagConstraints gbc_a7Btn = new GridBagConstraints();
		gbc_a7Btn.gridheight = 2;
		gbc_a7Btn.fill = GridBagConstraints.BOTH;
		gbc_a7Btn.insets = new Insets(0, 0, 5, 5);
		gbc_a7Btn.gridx = 4;
		gbc_a7Btn.gridy = 5;
		add(a7Btn, gbc_a7Btn);
		
		
		GridBagConstraints gbc_a8Btn = new GridBagConstraints();
		gbc_a8Btn.gridheight = 2;
		gbc_a8Btn.fill = GridBagConstraints.BOTH;
		gbc_a8Btn.insets = new Insets(0, 0, 5, 5);
		gbc_a8Btn.gridx = 5;
		gbc_a8Btn.gridy = 5;
		add(a8Btn, gbc_a8Btn);
		
		
		GridBagConstraints gbc_a9Btn = new GridBagConstraints();
		gbc_a9Btn.gridheight = 2;
		gbc_a9Btn.fill = GridBagConstraints.BOTH;
		gbc_a9Btn.insets = new Insets(0, 0, 5, 0);
		gbc_a9Btn.gridx = 6;
		gbc_a9Btn.gridy = 5;
		add(a9Btn, gbc_a9Btn);
		
		
		GridBagConstraints gbc_aguaBtn = new GridBagConstraints();
		gbc_aguaBtn.gridheight = 2;
		gbc_aguaBtn.fill = GridBagConstraints.BOTH;
		gbc_aguaBtn.insets = new Insets(0, 0, 5, 5);
		gbc_aguaBtn.gridx = 1;
		gbc_aguaBtn.gridy = 7;
		add(aguaBtn, gbc_aguaBtn);
		
		
		GridBagConstraints gbc_patatasBtn = new GridBagConstraints();
		gbc_patatasBtn.gridheight = 2;
		gbc_patatasBtn.fill = GridBagConstraints.BOTH;
		gbc_patatasBtn.insets = new Insets(0, 0, 5, 5);
		gbc_patatasBtn.gridx = 2;
		gbc_patatasBtn.gridy = 7;
		add(patatasBtn, gbc_patatasBtn);
		
		
		GridBagConstraints gbc_croquetasBtn = new GridBagConstraints();
		gbc_croquetasBtn.gridheight = 2;
		gbc_croquetasBtn.fill = GridBagConstraints.BOTH;
		gbc_croquetasBtn.insets = new Insets(0, 0, 5, 5);
		gbc_croquetasBtn.gridx = 3;
		gbc_croquetasBtn.gridy = 7;
		add(croquetasBtn, gbc_croquetasBtn);
		
		
		GridBagConstraints gbc_a4Btn = new GridBagConstraints();
		gbc_a4Btn.gridheight = 2;
		gbc_a4Btn.fill = GridBagConstraints.BOTH;
		gbc_a4Btn.insets = new Insets(0, 0, 5, 5);
		gbc_a4Btn.gridx = 4;
		gbc_a4Btn.gridy = 7;
		add(a4Btn, gbc_a4Btn);
		
		
		GridBagConstraints gbc_a5Btn = new GridBagConstraints();
		gbc_a5Btn.gridheight = 2;
		gbc_a5Btn.fill = GridBagConstraints.BOTH;
		gbc_a5Btn.insets = new Insets(0, 0, 5, 5);
		gbc_a5Btn.gridx = 5;
		gbc_a5Btn.gridy = 7;
		add(a5Btn, gbc_a5Btn);
		
		
		GridBagConstraints gbc_a6Btn = new GridBagConstraints();
		gbc_a6Btn.gridheight = 2;
		gbc_a6Btn.fill = GridBagConstraints.BOTH;
		gbc_a6Btn.insets = new Insets(0, 0, 5, 0);
		gbc_a6Btn.gridx = 6;
		gbc_a6Btn.gridy = 7;
		add(a6Btn, gbc_a6Btn);
		
		
		GridBagConstraints gbc_cafeBtn = new GridBagConstraints();
		gbc_cafeBtn.gridheight = 2;
		gbc_cafeBtn.fill = GridBagConstraints.BOTH;
		gbc_cafeBtn.insets = new Insets(0, 0, 5, 5);
		gbc_cafeBtn.gridx = 1;
		gbc_cafeBtn.gridy = 9;
		add(cafeBtn, gbc_cafeBtn);
		
		
		GridBagConstraints gbc_pizzaBtn = new GridBagConstraints();
		gbc_pizzaBtn.gridheight = 2;
		gbc_pizzaBtn.fill = GridBagConstraints.BOTH;
		gbc_pizzaBtn.insets = new Insets(0, 0, 5, 5);
		gbc_pizzaBtn.gridx = 2;
		gbc_pizzaBtn.gridy = 9;
		add(pizzaBtn, gbc_pizzaBtn);
		
		
		GridBagConstraints gbc_heladoBtn = new GridBagConstraints();
		gbc_heladoBtn.gridheight = 2;
		gbc_heladoBtn.fill = GridBagConstraints.BOTH;
		gbc_heladoBtn.insets = new Insets(0, 0, 5, 5);
		gbc_heladoBtn.gridx = 3;
		gbc_heladoBtn.gridy = 9;
		add(heladoBtn, gbc_heladoBtn);
		
		
		GridBagConstraints gbc_a1Btn = new GridBagConstraints();
		gbc_a1Btn.gridheight = 2;
		gbc_a1Btn.fill = GridBagConstraints.BOTH;
		gbc_a1Btn.insets = new Insets(0, 0, 5, 5);
		gbc_a1Btn.gridx = 4;
		gbc_a1Btn.gridy = 9;
		add(a1Btn, gbc_a1Btn);
		
		
		GridBagConstraints gbc_a2Btn = new GridBagConstraints();
		gbc_a2Btn.gridheight = 2;
		gbc_a2Btn.fill = GridBagConstraints.BOTH;
		gbc_a2Btn.insets = new Insets(0, 0, 5, 5);
		gbc_a2Btn.gridx = 5;
		gbc_a2Btn.gridy = 9;
		add(a2Btn, gbc_a2Btn);
		
		
		GridBagConstraints gbc_a3Btn = new GridBagConstraints();
		gbc_a3Btn.gridheight = 2;
		gbc_a3Btn.fill = GridBagConstraints.BOTH;
		gbc_a3Btn.insets = new Insets(0, 0, 5, 0);
		gbc_a3Btn.gridx = 6;
		gbc_a3Btn.gridy = 9;
		add(a3Btn, gbc_a3Btn);
		
		
		GridBagConstraints gbc_a0Btn = new GridBagConstraints();
		gbc_a0Btn.gridheight = 2;
		gbc_a0Btn.fill = GridBagConstraints.BOTH;
		gbc_a0Btn.insets = new Insets(0, 0, 5, 5);
		gbc_a0Btn.gridx = 4;
		gbc_a0Btn.gridy = 11;
		add(a0Btn, gbc_a0Btn);
		
		
		GridBagConstraints gbc_nuevoBtn = new GridBagConstraints();
		gbc_nuevoBtn.gridheight = 2;
		gbc_nuevoBtn.fill = GridBagConstraints.BOTH;
		gbc_nuevoBtn.insets = new Insets(0, 0, 5, 5);
		gbc_nuevoBtn.gridx = 5;
		gbc_nuevoBtn.gridy = 11;
		add(nuevoBtn, gbc_nuevoBtn);
		
		
		GridBagConstraints gbc_salirBtn = new GridBagConstraints();
		gbc_salirBtn.gridheight = 2;
		gbc_salirBtn.insets = new Insets(0, 0, 5, 0);
		gbc_salirBtn.fill = GridBagConstraints.BOTH;
		gbc_salirBtn.gridx = 6;
		gbc_salirBtn.gridy = 11;
		add(salirBtn, gbc_salirBtn);
		
	}
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		int total = 0;
		int numPedido=1;
		String info="";
		int cantidadTotal=0;
		AbstractButton totalTxt = null;
		int i = 0;
		// TODO Auto-generated method stub
		if(e.getSource()==a9Btn) {
            i = 9;
            iTxt.setText("9");
        }
        else if(e.getSource()==a8Btn) {
            i=8;
            iTxt.setText("8");
        }
        else if(e.getSource()==a7Btn){
            i=7;
            iTxt.setText("7");
        }
        else if(e.getSource()==a6Btn){
            i=6;
            iTxt.setText("6");
        }
        else if(e.getSource()==a5Btn){
            i=5;
            iTxt.setText("5");
        }
        else if(e.getSource()==a4Btn){
            i=4;
            iTxt.setText("4");
        }
        else if(e.getSource()==a3Btn){
            i=3;
            iTxt.setText("3");
        }
        else if(e.getSource()==a2Btn){
            i=2;
            iTxt.setText("2");
        }
        else if(e.getSource()==a1Btn){
            i=1;
            iTxt.setText("1");
        }
        else if(e.getSource()==a0Btn){
            i=0;
            iTxt.setText("0");
        }
        else if(e.getSource()==pizzaBtn){
            total=total+5*i;
            info=info+"\n"+numPedido+"   Producto: Pizza  " + "Precio/Ud:5   Cantidad:"+ i+"  Total:  "+  total+"";
            infoArea.setText(info);

            numPedido++;
            cantidadTotal=cantidadTotal+i;
            totalTxt.setText(""+cantidadTotal);
            i=1;
        }
        else if(e.getSource()==bocadilloBtn){
            total=total+3*i;
            info=info+"\n"+numPedido+"   Producto: Bocadillo  " + "Precio/Ud:3   Cantidad:"+ i+"  Total:  "+  total+"";
            infoArea.setText(info);

            numPedido++;
            cantidadTotal=cantidadTotal+i;
            totalTxt.setText(""+cantidadTotal);
            i=1;
        }
        else if(e.getSource()==heladoBtn){
            total=total+1*i;
            info=info+"\n"+numPedido+"   Producto: Helado  " + "Precio/Ud:1   Cantidad:"+ i+"  Total:  "+  total+"";
            infoArea.setText(info);

            numPedido++;
            cantidadTotal=cantidadTotal+i;
            totalTxt.setText(""+cantidadTotal);
            i=1;
        }
        else if(e.getSource()==cocacolaBtn){
            total=total+1*i;
            info=info+"\n"+numPedido+"   Producto: Cocacola  " + "Precio/Ud:1   Cantidad:"+ i+"  Total:  "+  total+"";
            infoArea.setText(info);

            numPedido++;
            cantidadTotal=cantidadTotal+i;
            totalTxt.setText(""+cantidadTotal);
            i=1;
        }
        else if(e.getSource()==fantaBtn){
            total=total+1*i;
            info=info+"\n"+numPedido+"   Producto: Fanta  " + "Precio/Ud:5   Cantidad:"+ i+"  Total:  "+  total+"";
            infoArea.setText(info);

            numPedido++;
            cantidadTotal=cantidadTotal+i;
            totalTxt.setText(""+cantidadTotal);
            i=1;
        }

        else if(e.getSource()==patatasBtn){
            total=total+3*i;
            info=info+"\n"+numPedido+"   Producto: Patatas  " + "Precio/Ud:3   Cantidad:"+ i+"  Total:  "+  total+"";
            infoArea.setText(info);

            numPedido++;
            cantidadTotal=cantidadTotal+i;
            totalTxt.setText(""+cantidadTotal);
            i=1;
        }
        else if(e.getSource()==croquetasBtn){
            total=total+4*i;
            info=info+"\n"+numPedido+"   Producto: Croquetas  " + "Precio/Ud:4   Cantidad:"+ i+"  Total:  "+  total+"";
            infoArea.setText(info);

            numPedido++;
            cantidadTotal=cantidadTotal+i;
            totalTxt.setText(""+cantidadTotal);
            i=1;
        }
        else if(e.getSource()==aguaBtn){
            total=total+1*i;
            info=info+"\n"+numPedido+"   Producto: Agua  " + "Precio/Ud:1   Cantidad:"+ i+"  Total:  "+  total+"";
            infoArea.setText(info);

            numPedido++;
            cantidadTotal=cantidadTotal+i;
            totalTxt.setText(""+cantidadTotal);
            i=1;
        }
        else if(e.getSource()==cafeBtn){
            total=total+1*i;

            info=info+"\n"+numPedido+"   Producto: Café  " + "Precio/Ud:1   Cantidad:"+ i+"  Total:  "+  total+"";
            infoArea.setText(info);

            numPedido++;
            cantidadTotal=cantidadTotal+i;
            totalTxt.setText(""+cantidadTotal);
            i=1;
        }
        else if(e.getSource()==nuevoBtn){
            infoArea.setText("");
            total=0;
            numPedido=1;
            i=1;
            info="";
            iTxt.setText("1");
            cantidadTotal=0;
            totalTxt.setText(""+cantidadTotal);

        }
        else if(e.getSource()==salirBtn){
            System.exit(1);
        }
		
	}

}
