package main;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Ventana extends JPanel implements ActionListener{
	
	String Espa�ol[] = {"Hola","Adios","Buenos dias","Coche","Loro",
            "Perro","Ordenador","Isla","Videojuego","Traductor"};
    String Ingles[] = {"Hello","GoodBye","Good Morning","Car","Parrot",
            "Dog","Computer","Island","Videogame","Translator"};
    String Frances[] = {"Salut","Adieu","Bonjour","Voiture","Perroquet",
            "Chien","Ordinateur","�le","Jeu vid�o","Traducteur"};
	
	private JTextField idiomaEntrada;
	private JTextField idiomaSalida;
	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	
	public Ventana(){
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblTraductor = new JLabel("Traduccion");
		GridBagConstraints gbc_lblTraductor = new GridBagConstraints();
		gbc_lblTraductor.gridwidth = 3;
		gbc_lblTraductor.insets = new Insets(0, 0, 5, 5);
		gbc_lblTraductor.gridx = 1;
		gbc_lblTraductor.gridy = 1;
		add(lblTraductor, gbc_lblTraductor);
		
		JLabel lblIdiomaOrigen = new JLabel("Idioma Origen");
		GridBagConstraints gbc_lblIdiomaOrigen = new GridBagConstraints();
		gbc_lblIdiomaOrigen.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdiomaOrigen.gridx = 6;
		gbc_lblIdiomaOrigen.gridy = 1;
		add(lblIdiomaOrigen, gbc_lblIdiomaOrigen);
		
		idiomaEntrada = new JTextField();
		idiomaEntrada.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_idiomaEntrada = new GridBagConstraints();
		gbc_idiomaEntrada.gridheight = 3;
		gbc_idiomaEntrada.gridwidth = 2;
		gbc_idiomaEntrada.insets = new Insets(0, 0, 5, 5);
		gbc_idiomaEntrada.fill = GridBagConstraints.HORIZONTAL;
		gbc_idiomaEntrada.gridx = 1;
		gbc_idiomaEntrada.gridy = 2;
		add(idiomaEntrada, gbc_idiomaEntrada);
		idiomaEntrada.setColumns(10);
		
		JRadioButton espanolRB = new JRadioButton("Espanol");
		buttonGroup.add(espanolRB);
		GridBagConstraints gbc_espanolRB = new GridBagConstraints();
		gbc_espanolRB.fill = GridBagConstraints.HORIZONTAL;
		gbc_espanolRB.insets = new Insets(0, 0, 5, 5);
		gbc_espanolRB.gridx = 6;
		gbc_espanolRB.gridy = 2;
		add(espanolRB, gbc_espanolRB);
		
		JRadioButton inglesRB = new JRadioButton("Ingles");
		buttonGroup.add(inglesRB);
		GridBagConstraints gbc_inglesRB = new GridBagConstraints();
		gbc_inglesRB.fill = GridBagConstraints.HORIZONTAL;
		gbc_inglesRB.insets = new Insets(0, 0, 5, 5);
		gbc_inglesRB.gridx = 6;
		gbc_inglesRB.gridy = 3;
		add(inglesRB, gbc_inglesRB);
		
		JRadioButton francesRB = new JRadioButton("Frances");
		buttonGroup.add(francesRB);
		GridBagConstraints gbc_francesRB = new GridBagConstraints();
		gbc_francesRB.fill = GridBagConstraints.HORIZONTAL;
		gbc_francesRB.insets = new Insets(0, 0, 5, 5);
		gbc_francesRB.gridx = 6;
		gbc_francesRB.gridy = 4;
		add(francesRB, gbc_francesRB);
		
		JLabel lblIdioma = new JLabel("Idioma");
		lblIdioma.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblIdioma = new GridBagConstraints();
		gbc_lblIdioma.gridwidth = 3;
		gbc_lblIdioma.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdioma.gridx = 1;
		gbc_lblIdioma.gridy = 6;
		add(lblIdioma, gbc_lblIdioma);
		
		JLabel lblIdiomaSalida = new JLabel("Idioma Salida");
		GridBagConstraints gbc_lblIdiomaSalida = new GridBagConstraints();
		gbc_lblIdiomaSalida.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdiomaSalida.gridx = 6;
		gbc_lblIdiomaSalida.gridy = 6;
		add(lblIdiomaSalida, gbc_lblIdiomaSalida);
		
		JRadioButton espanolRB1 = new JRadioButton("Espanol");
		buttonGroup_1.add(espanolRB1);
		GridBagConstraints gbc_espanolRB1 = new GridBagConstraints();
		gbc_espanolRB1.fill = GridBagConstraints.HORIZONTAL;
		gbc_espanolRB1.insets = new Insets(0, 0, 5, 5);
		gbc_espanolRB1.gridx = 6;
		gbc_espanolRB1.gridy = 7;
		add(espanolRB1, gbc_espanolRB1);
		
		idiomaSalida = new JTextField();
		GridBagConstraints gbc_idiomaSalida = new GridBagConstraints();
		gbc_idiomaSalida.gridheight = 3;
		gbc_idiomaSalida.gridwidth = 2;
		gbc_idiomaSalida.insets = new Insets(0, 0, 5, 5);
		gbc_idiomaSalida.fill = GridBagConstraints.HORIZONTAL;
		gbc_idiomaSalida.gridx = 1;
		gbc_idiomaSalida.gridy = 7;
		add(idiomaSalida, gbc_idiomaSalida);
		idiomaSalida.setColumns(10);
		
		JRadioButton inglesRB1 = new JRadioButton("Ingles");
		buttonGroup_1.add(inglesRB1);
		GridBagConstraints gbc_inglesRB1 = new GridBagConstraints();
		gbc_inglesRB1.fill = GridBagConstraints.HORIZONTAL;
		gbc_inglesRB1.insets = new Insets(0, 0, 5, 5);
		gbc_inglesRB1.gridx = 6;
		gbc_inglesRB1.gridy = 8;
		add(inglesRB1, gbc_inglesRB1);
		
		JRadioButton francesRB1 = new JRadioButton("Frances");
		buttonGroup_1.add(francesRB1);
		GridBagConstraints gbc_francesRB1 = new GridBagConstraints();
		gbc_francesRB1.insets = new Insets(0, 0, 5, 5);
		gbc_francesRB1.fill = GridBagConstraints.HORIZONTAL;
		gbc_francesRB1.gridx = 6;
		gbc_francesRB1.gridy = 9;
		add(francesRB1, gbc_francesRB1);
		
		JButton traducirB = new JButton("Traducir");
		GridBagConstraints gbc_traducirB = new GridBagConstraints();
		gbc_traducirB.insets = new Insets(0, 0, 0, 5);
		gbc_traducirB.gridx = 1;
		gbc_traducirB.gridy = 11;
		add(traducirB, gbc_traducirB);
		
	
	
	espanolRB.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
        	lblIdioma.setText("Idioma Espa�ol");
        }});
    inglesRB.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
        	lblIdioma.setText("Idioma Ingles");
        }});
    francesRB.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
        	lblIdioma.setText("Idioma Franc�s");
        }});

    traducirB.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            for (int i = 0;i<Ingles.length;i++) {
                if(espanolRB.isSelected()) {
                    if (idiomaEntrada.getText().equalsIgnoreCase(Espa�ol[i])) {
                        if (inglesRB1.isSelected()) {
                            idiomaSalida.setText(Ingles[i]);
                        } else if (francesRB1.isSelected()) {
                            idiomaSalida.setText(Frances[i]);
                        } else if (espanolRB1.isSelected()) {
                            idiomaSalida.setText(Espa�ol[i]);
                        }}
                    } else if (inglesRB.isSelected()) {
                        if (idiomaEntrada.getText().equalsIgnoreCase(Ingles[i])) {
                            if (inglesRB1.isSelected()) {
                                idiomaSalida.setText(Ingles[i]);
                            } else if (francesRB1.isSelected()) {
                                idiomaSalida.setText(Frances[i]);
                            } else if (espanolRB1.isSelected()) {
                                idiomaSalida.setText(Espa�ol[i]);
                            }}
                        } else if (francesRB.isSelected()) {
                            if (idiomaEntrada.getText().equalsIgnoreCase(Frances[i])) {
                                if (inglesRB1.isSelected()) {
                                    idiomaSalida.setText(Ingles[i]);
                                } else if (francesRB1.isSelected()) {
                                    idiomaSalida.setText(Frances[i]);
                                } else if (espanolRB1.isSelected()) {
                                    idiomaSalida.setText(Espa�ol[i]);
                                }
                            }

                        }


                    }


        }

    });

	
	

}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
